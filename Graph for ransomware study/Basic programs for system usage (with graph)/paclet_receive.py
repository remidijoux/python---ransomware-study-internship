import psutil, time
import matplotlib.pyplot as plt


packet_list_receive = []

for i in range(5):
    p = psutil
    packetreceive = p.net_io_counters().packets_recv
    packet_list_receive.append(packetreceive)
    time.sleep(10)

print(packet_list_receive)

plt.plot(packet_list_receive, 'r')

plt.ylabel('Number of packets received')
plt.xlabel('Time in second')
plt.show()


