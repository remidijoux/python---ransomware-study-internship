import psutil, time
import matplotlib.pyplot as plt

packet_list_send = []

for i in range(5):
    p = psutil
    packetsend = p.net_io_counters().packets_sent
    packet_list_send.append(packetsend)
    time.sleep(10)

print(packet_list_send)

plt.plot(packet_list_send, 'b')

plt.ylabel('Number of packets received')
plt.xlabel('Time in second')
plt.show()


