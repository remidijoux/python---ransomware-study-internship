import psutil
import matplotlib.pyplot as plt


memoire_list = []
for i in range(100):
    p = psutil
    p_memory = p.virtual_memory().percent
    memoire_list.append(p_memory)

print(memoire_list)

plt.plot(memoire_list)
plt.ylabel('Memory Usage')
plt.show()


