# ------ Libraries imports ------- #
import psutil
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

# ------ Function in order to animate our graph
def animate(i):

    # ------ Adding the new values in the list ------ #
    ys_packets_received.append(psutil.net_io_counters().packets_recv)
    ys_packets_sended.append(psutil.net_io_counters().packets_sent)

    # ------ Clearing the axis ------ #
    ax1.clear()

    # ------ Putting Label for the graph ------ #
    plt.ylabel('Number of Packets')
    plt.xlabel('Time in second')

    # ------ Plotting values in axis ------ #
    ax1.plot(ys_packets_received)
    ax2.plot(ys_packets_sended)

    # ------ Axis limits ------ #
    ax1.set_xlim([0, 300])
    #ax1.set_ylim([0, 100])

    # ------ Labels used for axis ------ #
    ax1.legend(['PACKETS RECEIVED', 'PACKETS SENDED'])

# ------ Graph Style used ------ #
style.use('fivethirtyeight')

# ------ Plotting the Graph ------ #
fig = plt.figure()

# ------ Plotting other graph for the CPU, MEMORY & DISK USAGE ------ #
ax1 = fig.add_subplot(111)
ax2 = fig.add_subplot(111)

# ------ We initialize three lists in order to stock values
# ~ List for the packets received ~ #
ys_packets_received = []
# ~ List for the packets sended ~ #
ys_packets_sended = []

# ------ Setup the animation with a 200milliseconds interval ------ #
ani = animation.FuncAnimation(fig, animate, interval=200)
# ------ Plotting the graph ------ #
plt.show()